#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 11:13:49 2024

@author: angelmario


------------------------------------------------------------
Algoritmo del programa:
    1. Se muestra el menú
    2. Se solicita la opción
    3. La opción lleva al módulo indicado
    4. La última opción, sale del sistema

------------------------------------------------------------
Algoritmo General del Método de Bisección:
    1. Definir los extremos del intervalo [num_a, num_b].
    2. Establecer si num_a o num_b son raices.
        - Si alguna es raíz, el programa termina.
    3. Revisar si existe cambio de signo en el intervalo [num_a, num_b].
        - Si no existe cambio de signo, el programa termina.
    4. Repetir según el número de iteraciones.
        - Se calcula el valor_x con el método seleccionado (Bisección o Falsa Posición).
        - Se calcula el valor de f(valor_x).
        - Se revisa si f(valor_x) es raíz: Si es raíz, se finaliza.
        - Se define el nuevo resultado [num_a, num_b]
    5. Imprimir resultados y regresar al menú.
------------------------------------------------------------
"""

def f(x):
    #define la función a desarrollar (f(x))
    #usar ** para elevar a un número
    #x^5+7x-6 sería (x**5) + x*7 - 6
    #return (x**9) + x*6 - (x**3) + 2    
    return (x**5) + x*7 - 6    
 
def menu_principal():
    #Menú que se muestra al iniciar el programa
    opciones = {
        '1': ('Informacion de esta aplicación / proyecto', info_app),
        '2': ('Método de Bisección', app_biseccion),
        '3': ('Salir de la app.', salir)
    }
    generar_menu(opciones, '3')

def generar_menu(opciones, opcion_salida):
    #Función que genera el menú
    opcion = None
    while opcion != opcion_salida:
        mostrar_menu(opciones)
        opcion = leer_opcion(opciones)
        ejecutar_opcion(opcion, opciones)
        print()


def mostrar_menu(opciones):
    #Impresión del menú en pantalla

    print("\033[H\033[J") 
    print('\x1b[1;30;47m' +          "    __  ___ __  __            __              _   __                 __       _                " + '\x1b[0m')
    print('\x1b[1;30;47m' +          "   /  |/  //_/ / /_____  ____/ /___  _____   / | / /_  ______ ___  _/_/ _____(_)________  _____" + '\x1b[0m')
    print('\x1b[1;30;47m' +          "  / /|_/ / _ \/ __/ __ \/ __  / __ \/ ___/  /  |/ / / / / __ `__ \/ _ \/ ___/ / ___/ __ \/ ___/" + '\x1b[0m')
    print('\x1b[1;30;47m' +          " / /  / /  __/ /_/ /_/ / /_/ / /_/ (__  )  / /|  / /_/ / / / / / /  __/ /  / / /__/ /_/ (__  ) " + '\x1b[0m')
    print('\x1b[1;30;47m' +          "/_/  /_/\___/\__/\____/\__,_/\____/____/  /_/ |_/\__,_/_/ /_/ /_/\___/_/  /_/\___/\____/____/  " + '\x1b[0m')
    print('\x1b[1;35;47m' +          'Carnet 1490-12-19404 Angel Mario de León Recinos | Cat.:Ing. Víctor Mendoza | 5to Semestre 2024' + '\x1b[0m')
    print()
    print()
    print()
    print('\x1b[1;33;44m' + ':::: Menú Principal ::::' + '\x1b[0m')
    print()

    for clave in sorted(opciones):
        print(f' {clave}) {opciones[clave][0]}')


def leer_opcion(opciones):
    while (a := input('Opción [1..3]: ')) not in opciones:
        print('')
        print('')
        print('\x1b[1;33;41m' +'Opción incorrecta, vuelva a intentarlo.'+ '\x1b[0m')
        print('')
        print('')
    return a


def ejecutar_opcion(opcion, opciones):
    opciones[opcion][1]()


def salir():
    print('')
    print('')
    print('\x1b[1;37;44m' +
          'Gracias ingeniero por su empeño y dedicación para formar profesionales' + '\x1b[0m')
    print('\x1b[1;30;44m' +
          'Carnet 1490-12-19404 - Angel Mario de León Recinos - Métodos Numéricos' + '\x1b[0m')
    
    
    
def info_app():
    print('')
    print('')
    print('\x1b[1;37;44m' +
          'Proyecto Final del curso de Métodos Numéricos impartido por el Ing. Víctor Méndoza' + '\x1b[0m')
    print('\x1b[1;30;44m' +
          'Carnet 1490-12-19404 - Angel Mario de León Recinos - Métodos Numéricos' + '\x1b[0m')
    input("Presione ENTER para continuar...")


def cambio_signo(a, b):
    #verifica cambio de signo (comparando si es mayor o menor que cero)
    return (f(a) > 0 > f(b)) or (f(a) < 0 < f(b))

def si_es_raiz(x):
    #verifica si es raiz el parámetro
    return True if (f(x) == 0) else False


def validar_tipo_entero(mensaje: str):
#verifica que se ingrese un número tipo entero
    while True:
        try:
            numero = int(input(str(mensaje)))
            return numero
        except ValueError:
            print('\x1b[1;33;41m' +':::ERROR::: No se puede representar el dato como un número entero.')
            
def validar_tipo_flotante(mensaje: str):
#verifica que se ingrese un número tipo flotante
    while True:
        try:
            numero = float(input(str(mensaje)))
            return numero
        except ValueError:
            print('\x1b[1;33;41m' +':::ERROR::: No se puede representar el dato como un número.'+ '\x1b[0m')
            
          
def obtener_cantidad_interacciones():
    numero_de_iteraciones = 0
    while numero_de_iteraciones <= 0:
        numero_de_iteraciones = validar_tipo_entero("Ingrese el número de iteraciones que desea: ")

        if numero_de_iteraciones <= 0:
            print('\x1b[1;33;41m' +':::ERROR::: El número de iteraciones no puede ser <= cero.'+ '\x1b[0m')

    return numero_de_iteraciones
            
            
def aproximacion_raiz_x_interacciones(a: float, b: float, numero_de_iteraciones: int, metodo: int):

    metodo_elegido = {
        "Bisección": 0,
        #se puede dejar abierto a tener más métodos numéricos para su resolución
    }

    matriz_tabla_resultado = ['n', 'a', 'b', 'valor_x', 'f(valor_x)']
    row_format ="{:>15}" * (len(matriz_tabla_resultado) + 1)
    print(row_format.format("", *matriz_tabla_resultado))

    num_a = a
    num_b = b
    valor_x = 0.0
    iteraciones = 0

    if (si_es_raiz(num_a)) or (si_es_raiz(num_b)):
        if si_es_raiz(num_a):
            print("El extremo del intervalo num_a = ", num_a, " es raíz exacta")
            input("Presione ENTER para continuar...")

        if si_es_raiz(num_b):
            print("El extremo del intervalo num_b = ", num_b, " es raíz exacta")
            input("Presione ENTER para continuar...")

        return None

    if (metodo == metodo_elegido["Bisección"]):
        if not cambio_signo(num_a, num_b):
            print('\x1b[1;33;41m' +'ERROR: No hay cambio de signo en el intervalo..'+ '\x1b[0m')
            input("Presione ENTER para continuar...")
            return None

        while iteraciones < numero_de_iteraciones:
            if metodo == metodo_elegido["Bisección"]:
                valor_x = (num_a + num_b) / 2
            fvalor_x = f(valor_x)

            
            matriz_tabla_resultado =[iteraciones, num_a, num_b, valor_x, fvalor_x]
            print(row_format.format("", *matriz_tabla_resultado))
#            matriz_tabla_resultado.add_row([iteraciones, num_a, num_b, valor_x, fvalor_x])

            if fvalor_x == 0:
                break
            else:
                iteraciones += 1

            num_b = valor_x if (f(num_a) * fvalor_x < 0) else num_b
            num_a = valor_x if (f(num_b) * fvalor_x < 0) else num_a

        row_format ="{:>15}" * (len(matriz_tabla_resultado) + 1)
 #       print(matriz_tabla_resultado)
        print('\x1b[1;33;44m', "La mejor aproximación es valor_x = ", valor_x, '\x1b[0m', "\n\n")

    else:
        print('\x1b[1;33;41m' +'::ERROR:: No es posible calcular valor_x por el método indicado.'+ '\x1b[0m')



def app_biseccion():

    print("\033[H\033[J") 
    print(":::: APROXIMAR RAICES ::::\n")

    #print("\033[H\033[J") 
    a = validar_tipo_flotante("\nIntroduce el valor de a: ")
    if si_es_raiz(a):
        print("La raíz es: ", a)
        input("Presione ENTER para continuar...")
        return None

    b = validar_tipo_flotante("Introduce el valor de b: ")
    if si_es_raiz(b):
        print("La raíz es: ", b)
        input("Presione ENTER para continuar...")
        return None


    #print("\033[H\033[J") (código para "limpiar" la pantalla)
    if cambio_signo(a, b):
        num_iteraciones = obtener_cantidad_interacciones()
        print(":::: Interacciones ::::\n")
        aproximacion_raiz_x_interacciones(a, b, num_iteraciones, 0)

    else:
        print('\x1b[1;33;41m' +"ERROR: No hay cambio de signo en el intervalo.[", a, ", ", b, "]" + '\x1b[0m')
        input("Presione ENTER para continuar...")

    input("Método proceso exitosamente. Presione ENTER para continuar...")


 
if __name__ == '__main__':
    print('')



    menu_principal()