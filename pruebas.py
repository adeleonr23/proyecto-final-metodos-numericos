#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 20:16:05 2024

@author: angelmario
"""


print("\033[H\033[J") 

tabla_resultados = ['n', 'a', 'b', 'x_n', 'f(x_n)']
row_format ="{:>15}" * (len(tabla_resultados) + 1)
print(row_format.format("", *tabla_resultados))

